<?php

namespace Drupal\fhrs_api\Controller;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Serialization\Json;

/**
 * Class ApiController.
 *
 * @package Drupal\fhrs_api\Controller
 */
class ApiController extends ControllerBase {

  /**
   * List all establishments.
   *
   * @todo: Rethink caching strategy. Now caching until next cache-clear...
   *
   * @return array
   *   Return list of all FHRS establishments
   */
  public function establishments() {

    if (isset($_GET['name'])) {
      $name = Html::escape($_GET['name']);
    }
    else {
      $name = '';
    }

    if (isset($_GET['rating'])) {
      $rating = Html::escape($_GET['rating']);
    }
    else {
      $rating = '';
    }

    $filters = [
      'name' => $name,
      'rating' => $rating,
    ];

    $cid = 'fhrs_api:totalCount' . $name . $rating;
    if ($cache = $this->cache()->get($cid)) {
      $count = $cache->data;
    }
    else {
      // Get total count of establishments and prepare pager.
      $count = new GetFhrsContent();
      $count = $count->totalCount($filters);
      $this->cache()->set($cid, $count);
    }

    pager_default_initialize($count, 100);
    $pageNumber = pager_find_page() + 1;
    $pageSize = 100;

    // Pulling teh data from FHRS API takes quite a while, cache each page.
    $render = &drupal_static(__FUNCTION__);

    // Attach the filtering form.
    $render[] = $this->formBuilder()->getForm('\Drupal\fhrs_api\Form\FhrsFilterForm');

    $cid = 'fhrs_api:rt' . $rating . ':nm' . $name . ':pn' . $pageNumber;
    if ($cache = $this->cache()->get($cid)) {
      $render = $cache->data;
    }
    else {
      // ...and if not cached get the stuff to render array.
      $get_fhrs = new GetFhrsContent();
      $establishments = $get_fhrs->establishments($pageNumber, $pageSize, $filters);

      // Define keys here as machine names of FHRS rating result keys.
      $tableheader = [
        '#',
        'FHRSID',
        'LocalAuthorityBusinessID',
        'BusinessName',
        'BusinessType',
        'RatingValue',
        'RatingDate',
      ];

      $rows = [];
      foreach ($establishments['establishments'] as $key => $item) {
        $n = $key + 1;
        if ($pageNumber > 1) {
          $n = $n + ($pageNumber * $pageSize);
        }

        $url = Url::fromRoute('fhrs_api.api_controller_establishment', ['fhrsid' => $item[$tableheader[1]]]);
        // Build rows with custom id + same keys as in table header.
        $rows[] = [
          $n,
          Link::fromTextAndUrl($item[$tableheader[1]], $url),
          $item[$tableheader[2]],
          $item[$tableheader[3]],
          $item[$tableheader[4]],
          $item[$tableheader[5]],
          $item[$tableheader[6]],
        ];
      }

      $render[] = [
        '#type' => 'table',
        '#header' => $tableheader,
        '#rows' => $rows,
        '#attributes' => [
          'id' => 'foo',
        ],
      ];
      $render[] = ['#type' => 'pager'];

      $this->cache()->set($cid, $render);
    }

    return $render;
  }

  /**
   * Establishment.
   *
   * @return array
   *   Return A single FHRS establishment
   */
  public function establishment($fhrsid) {

    // Se messages array for later use in case of errors.
    $messages['notfound'] = $this->t('No establishments found with FHRSID "<code>@fhrsid</code>"', ['@fhrsid' => $fhrsid]);

    if (!is_numeric($fhrsid)) {
      return [
        '#type' => 'markup',
        '#markup' => $messages['notfound'],
      ];
    }

    $get_fhrs = new GetFhrsContent();
    $establishment = $get_fhrs->establishment($fhrsid);
    $content = '<p></p><a href="/fhrs/establishments">Back to list</a></p>';

    // In case we get empty array.
    if (!is_array($establishment)) {
      return [
        '#type' => 'markup',
        '#markup' => $messages['notfound'],
      ];
    }

    // Parse through the json, keys as labels.
    $content .= '<ul>';
    foreach ($establishment as $key => $value) {
      if ($key == 'meta') {
        continue;
      }
      if (!is_array($value)) {
        $content .= '<li>' . $key . ': ' . $value . '</li>';
      }
      else {
        $content .= '<li>' . $key . ':<ul>';

        foreach ($value as $subkey => $subvalue) {
          $content .= '<li>' . $subkey . ': ' . $subvalue . '</li>';
        }
        $content .= '</ul></li>';
      }
    }
    $content .= '</ul>';

    return [
      '#type' => 'markup',
      '#markup' => $content,
    ];
  }

  /**
   * Get single value from establishment item.
   *
   * @param int $fhrsid
   *   FHRS ID.
   * @param string $key
   *   A kay from Establishment array.
   *
   * @return string
   *   Requested establishment array item value.
   */
  public function getEstablishmentValue($fhrsid, $key) {
    $get_fhrs = new GetFhrsContent();
    $establishment = $get_fhrs->establishment($fhrsid);
    return $establishment[$key];
  }

  /**
   * Set Establishment page title.
   *
   * @param int $fhrsid
   *   FHRS ID.
   *
   * @return string
   *   Establishment ppage title
   */
  public function establishmentTitle($fhrsid) {
    if (is_numeric($fhrsid)) {
      $businessname = ApiController::getEstablishmentValue($fhrsid, 'BusinessName');
      $title = $this->t('FHRS') . ' ' . $fhrsid . ': ' . $businessname;
    }
    else {
      $title = $this->t('Error fetching establishmment.');
    }

    return $title;
  }

}

/**
 * Class GetFhrsContent.
 *
 * @package Drupal\fhrs_api\Controller
 */
class GetFhrsContent {

  /**
   * FHRS API required headers.
   *
   * @return array
   *   Headers
   */
  private function headers() {
    $headers = [
      'headers' => [
        'accept' => 'application/json',
        'x-api-version' => 2,
      ],
    ];
    return $headers;
  }

  /**
   * Get status of FHRS API.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup|int
   *   Status code or error.
   */
  public static function status() {

    // @todo: URL to configs
    $url = 'http://api.ratings.food.gov.uk';

    $client = new Client();
    try {
      $res = $client->get($url, ['http_errors' => FALSE]);
      return($res->getStatusCode());
    }
    catch (RequestException $e) {
      return t('Status check failed');
    }
  }

  /**
   * Get total count of FHRS items.
   *
   * @param array $filters
   *   Array of filters.
   *
   * @return int
   *   Total count
   */
  public function totalCount(array $filters = []) {

    $url = 'http://api.ratings.food.gov.uk/Establishments?pageSize=1&name=' . $filters['name'] . '&ratingKey=' . $filters['rating'];
    $headers = $this->headers();

    $client = \Drupal::httpClient();
    try {
      $res = $client->get($url, $headers);
      $count = Json::decode($res->getBody());
      $count = $count['meta']['totalCount'];
      return $count;
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

  /**
   * Get FHRSA Establishments.
   *
   * @param int $pageNumber
   *   Page number to get.
   * @param int $pageSize
   *   Size of items per page.
   * @param array $filters
   *   Array of filters for filtering results.
   *
   * @return bool|mixed
   *   Json decoded response.
   */
  public function establishments($pageNumber = 1, $pageSize = 100, array $filters = []) {

    // @todo: URL to configs
    // @todo: form for filtering params.
    $url = 'http://api.ratings.food.gov.uk/Establishments?pageNumber=' . $pageNumber . '&pageSize=' . $pageSize . '&name=' . $filters['name'] . '&ratingKey=' . $filters['rating'];
    $headers = $this->headers();

    $client = \Drupal::httpClient();
    try {
      $res = $client->get($url, $headers);
      return(Json::decode($res->getBody()));
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

  /**
   * Get establishment details.
   *
   * @param int $fhrsid
   *   FSHR API Establishment ID.
   *
   * @return bool|mixed
   *   JSON decoded response of establishment details.
   */
  public function establishment($fhrsid) {

    // @todo: URL to configs
    $url = 'http://api.ratings.food.gov.uk/Establishments/' . $fhrsid;
    $headers = $this->headers();

    $client = \Drupal::httpClient();
    try {
      $res = $client->get($url, $headers);
      return(Json::decode($res->getBody()));
    }
    catch (RequestException $e) {
      return FALSE;
    }
  }

}
