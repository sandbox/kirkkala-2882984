<?php

namespace Drupal\fhrs_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FhrsFilterForm.
 *
 * @package Drupal\fhrs_api\Form
 */
class FhrsFilterForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fhrs_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $user_input = $form_state->getUserInput();

    if (!isset($user_input['name'])) {
      $user_input['name'] = FALSE;
    }
    if (!isset($user_input['rating'])) {
      $user_input['rating'] = FALSE;
    }

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => isset($_GET['name']) ? $_GET['name'] : '',
      '#maxlength' => 64,
    ];
    $form['rating'] = [
      '#type' => 'select',
      '#title' => $this->t('Rating'),
      '#default_value' => isset($_GET['rating']) ? $_GET['rating'] : '',
      '#options' => [
        'n/a' => $this->t('- any -'),
        0 => 0,
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
      ],
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // If we ever want to validate anything.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $params = [];
    $name = $form_state->getValue('name');
    if ($name != '') {
      $params['name'] = $name;
    }

    $rating = $form_state->getValue('rating');
    if ($rating != 'n/a') {
      $params['rating'] = $rating;
    }
    else {
      $rating = NULL;
    }

    // Add values to current url as params.
    $form_state->setRedirect(
      \Drupal::routeMatch()->getRouteName(),
        $params
    );

  }

}
