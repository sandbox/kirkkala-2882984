INTRODUCTION
------------

The FHRS API module searches and displays ratings from Food Standards Agency
(FSA) Food Hygiene Rating Scheme (FHRS) API (Version 2)
http://api.ratings.food.gov.uk/help

 * For a full description of the module, visit the project page:
   https://drupal.org/project/fhrs_api

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/fhrs_api

REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - View FHRS API content

     Allows viewing the content at /fhrs/*

 * FHRS API module (8.x-1.0) currently has no other configuration options.


 MAINTAINERS
 -----------

 Current maintainers:
  * Timo Kirkkala - https://www.drupal.org/user/390806
